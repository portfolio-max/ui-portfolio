/* eslint-disable array-callback-return */
const express = require('express');
const next = require('next');
const path = require('path');
const compression = require('compression');
const cookieParser = require('cookie-parser');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const routes = require('../routes');

const handle = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = express();
    server.use(compression());
    // set localization next-i18n
    // server.use(nextI18NextMiddleware(nextI18next));
    server.use(express.static('static'));
    // server.use(express.static(path.join(__dirname, 'locales')));
    server.use(cookieParser());
    // server.use(morgan('combined', {
    //   skip(req, res) { return res.statusCode < 400; },
    // }));
    server.use((req, res, nex) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
      nex();
    });

    routes.map((v) => {
      server.get(
        `${v.slug}`,
        (req, res) => app.render(req, res, `${v.path}`, Object.assign({}, req.query, req.param)),
      );
    });

    server.get('/sitemap.xml', (req, res) => {
      res.send(200).sendFile('sitemap.xml', {
        root: path.join(__dirname, '/static/'),
        headers: {
          'Content-Type': 'application/xml',
        },
      });
    });

    server.get('/robots.txt', (req, res) => {
      res.send(200).sendFile('robots.txt', {
        root: path.join(__dirname, '/static/'),
        headers: {
          'Content-Type': 'text/plain;charset=UTF-8',
        },
      });
    });

    server.get('/service-worker.js', (req, res) => {
      res.sendFile(`${path.resolve(__dirname, '.', 'static')}/service-worker.js`);
    });
    server.get('*', (req, res) => handle(req, res));

    server.listen(process.env.APP_PORT || 3000, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${process.env.APP_PORT}`);
      console.log('> route list ', routes);
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
