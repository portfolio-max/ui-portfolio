import React, { useState } from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import ProgressBar from 'react-bootstrap/ProgressBar'
import Charts from './chart'
import Bar from './bar'
import Axios from 'axios';

const Started = ({ nextStep = () => console.log('not implemented') }) => (
    <div className="p-5">
        <span style={{ fontSize: 30 }}>Get Your Personality Insight</span>
        <div style={{ fontSize: 15, width: 300, marginTop: 20 }}>Fitts's law is a predictive model of human movement primarily used in human–computer interaction and ergonomics.</div>
        {/* <button className="btn btn-outline-primary mt-5" style={{ fontSize: 15}}>Get Started</button> */}
        <button onClick={() => nextStep('socialmedia')} className="footer__button-hire">
            Get Started
        </button>
    </div>
);

const MediaSocial = ({ nextStep = () => console.log('not implemented'), setPerc }) => {
    const responseFacebook = async (response) => {
        const accessToken = response.accessToken || '';
        const userID = response.userID || '';

        nextStep('isLoading')
        const resp = await Axios.post('http://128.199.241.34:8000/api/personality/', {
            "user_id": userID,
            "token": accessToken
        })

        setPerc(resp.data.data.percentiles)
        nextStep('result')
        console.log('response : ', resp);
    }
    return (
        <div className="p-5">
            <span style={{ fontSize: 30 }}>Get Your Personality Insight</span>
            <div style={{ fontSize: 15, width: 300, marginTop: 20 }}>Choose your social media account</div>
            {/* <button onClick={() => nextStep('isLoading')} className="footer__button-hire">
                Facebook
            </button> */}
            <FacebookLogin
                appId={'718360498927148'}
                callback={responseFacebook}
                fields="first_name,last_name,email,picture,user_posts"
                scope="email,public_profile"
                render={renderProps => (
                <button
                    onClick={() => {
                        renderProps.onClick()
                    }} className="footer__button-hire">
                    Facebook
                </button>
                )}
            />
            <button onClick={() => nextStep('isLoading')} className="footer__button-hire">
                Twitter
            </button>
        </div>
    )
}

const LoadingSkeleton = () => (
    <div className="p-5">
        <span style={{ fontSize: 30 }}>Get Your Personality Insight</span>
        <div style={{ fontSize: 15, width: 300, marginTop: '5em' }}>Starting analyse your personality …</div>
        <ProgressBar now={60} />
    </div>
);

const defaultState = {
    started: false,
    socialmedia: false,
    isLoading: false,
    result: false,
}

export default ({}) => {
    const [stepState, onChangeStep] = useState({
        started: true,
        socialmedia: false,
        isLoading: false,
        result: false,
    });

    const [perc, setPerc] = useState({});

    const nextStep = next => {
        onChangeStep({
            ...defaultState,
            [next]: true
        })
    }

    console.log('percentiles : ', perc);
    return (
        <div className="row mt-5 mb-5">
            <div className="col-lg-5 col-md-5 col-xs-12">
                { stepState.started && <Started nextStep={nextStep} />}
                { stepState.socialmedia && <MediaSocial nextStep={nextStep} setPerc={setPerc} /> }
                { stepState.isLoading && <LoadingSkeleton nextStep={nextStep} /> }
                { stepState.result && <Bar nextStep={nextStep} data={perc} />}
            </div>
            <div className="col-5 col-md-5 col-xs-12">
                <img src='../../../static/group.png' width={400} />
            </div>
        </div>
    )
}