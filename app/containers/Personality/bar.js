import { Progress } from 'antd';
import _get from 'lodash/get';
import { Row, Col, Container } from 'react-bootstrap';

const BarProgress = ({ data }) => {
    console.log('data : ', data);
    const def = {
        'pred_sAGR': 'Agreeableness',
        'pred_sCON': 'Conscientiousness',
        'pred_sEXT': 'Extraversion',
        'pred_sNEU': 'Neuroticism',
        'pred_sOPN': 'Openness',
    }
    let rebuild = []
    for (var k of Object.keys(data)) {
        const change = {
            label: def[k],
            value: data[k]
        }
        rebuild.push(change)
    }
    console.log('rebuild : ', rebuild);

    return (
        <Container className="ml-2">
            <span className="font-weight-bold pl-2 pr-2" style={{ fontSize: 25 }}>
                BIG 5 Personality
            </span>
            {rebuild.length > 0 && rebuild.map((v, k) => (
                <Row key={`personality_data-`}>
                    <Col lg={5} className="align-items-center d-flex font">
                        <span style={{ fontSize: 14 }}>{v.label}: </span>
                    </Col>
                    <Col lg={7}>
                        <Progress
                            percent={v.value}
                            strokeWidth={30}
                            strokeColor="#5AAAFA"
                            className="mt-3 mb-3"
                            format={score => `${Math.floor(score)}%`}
                        />
                    </Col>
                </Row>
            ))}
        </Container>
    )
}

export default BarProgress;