import React from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

 
class SocialButton extends React.Component {
    responseFacebook = (response) => {
        console.log(response);
    }
    render() {
        return (
            <FacebookLogin
                appId={this.props.appId}
                callback={this.responseFacebook}
                fields="first_name,last_name,email,picture"
                scope="email,public_profile"
                render={renderProps => (
                <div
                    onClick={renderProps.onClick}
                    backgroundColor="#3b5998"
                    marginTop="10"
                >
                    facebook
                </div>
                )}
            />
        );
    }
}
 
export default SocialButton;