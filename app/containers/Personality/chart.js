import dynamic from 'next/dynamic'
const ReactApexChart = dynamic(() => import('react-apexcharts'), {ssr:false})
class ApexChart extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
      
        series: [76, 67, 61, 90],
        options: {
          chart: {
            height: 390,
            type: 'radialBar',
          },
          plotOptions: {
            radialBar: {
              dataLabels: {
                name: {
                  fontSize: '22px',
                },
                value: {
                  fontSize: '16px',
                },
                total: {
                  show: true,
                  label: 'Result Personality Insight',
                  formatter: function (w) {
                    // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                    return ''
                  }
                }
              }
            }
          },
        //   colors: ['#1ab7ea', '#0084ff', '#39539E', '#0077B5'],
          labels: ['Openness', 'Conscientiousness', 'Extraversion', 'Agreeableness', 'Neuroticism'],
        //   legend: {
        //     show: true,
        //     floating: true,
        //     fontSize: '16px',
        //     position: 'left',
        //     offsetX: 20,
        //     offsetY: 30,
        //     labels: {
        //       useSeriesColors: true,
        //     },
        //     markers: {
        //       size: 0
        //     },
        //     formatter: function(seriesName, opts) {
        //       return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
        //     },
        //     itemMargin: {
        //       vertical: 3
        //     }
        //   },
        //   responsive: [{
        //     breakpoint: 480,
        //     options: {
        //       legend: {
        //           show: false
        //       }
        //     }
        //   }]
        },
      
      
      };
    }

  

    render() {
        const { data } = this.props;
        const def = {
            'pred_sAGR': 'Agreeableness',
            'pred_sCON': 'Conscientiousness',
            'pred_sEXT': 'Extraversion',
            'pred_sNEU': 'Neuroticism',
            'pred_sOPN': 'Openness',
        }
        console.log('data : ', data);
        let score = []
        if (data) {
            score = Object.values(data)
        }
        console.log('score : ', score);

        return (
            <div id="chart">
                <ReactApexChart options={this.state.options} series={score || this.state.series} type="radialBar" height={500} />
            </div>
        );
    }
  }

  export default ApexChart;