import { Row, Col } from 'react-bootstrap';

export default ({

}) => {
    const redirect = (url = '') => {
      window.open(url, '_blank');
    }
    return (
      <div className="mb-3 mt-lg-5">
        <Row>
          <Col
            className="pt-3 pb-3"
            xs={12}
            md={6}
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignContent: 'center'
            }}
          >
            <img
              className="landing-page__profilePic"
              src="../../static/pict.jpg"
            />
          </Col>
          <Col
            className="pt-3 pb-3"
            xs={12}
            md={6}
          >
            <div className="source-sans-pro">
              <div className="landing-page__profileDetail">
                <h4 className="landing-page__profileDetail-title">Hi,</h4>
                <h4 className="landing-page__profileDetail-name">I'm Maximilliano.</h4>
                <h3 className="landing-page__profileDetail-jobtitle">Full Stack Web Developer</h3>
                <h6 className="landing-page__profileDetail-desc">
                Software Engineer who mostly works as the full stack developer.
                Skills in designing and developing API,
                for server-client, created frontend with HTML, CSS Framework and ReactJs.
                </h6>
              </div>
              <div className="landing-page__profileDetail-dev-profile">
                <i style={{ margin: '0px 10px', cursor: 'pointer' }} className="fab fa-github" onClick={() => redirect('https://github.com/MaximillianoNico')} />
                <i style={{ margin: '0px 10px', color: '#F16A22', cursor: 'pointer' }} className="fab fa-gitlab" onClick={() => redirect('https://gitlab.com/MaximillianoNico')} />
                <i style={{ margin: '0px 10px', color: '#0D9EC6', cursor: 'pointer' }} className="fab fa-bitbucket" />
                <span style={{ paddingLeft: 15, color: '#9F9595' ,fontSize: 17 }}>More ...</span>
              </div>
            </div>
        </Col>
        </Row>
      </div>
    )
}