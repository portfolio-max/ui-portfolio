import { Row, Col } from 'react-bootstrap';
import { Fragment } from 'react';
export default () => {
  const staticDataExp = [
    {
      company: 'Tiket.com',
      from: 'Oct 2020',
      to: 'Now',
      title: 'Frontend Engineer',
      img: "https://media-exp1.licdn.com/dms/image/C560BAQFN5mZsxgIpwQ/company-logo_100_100/0?e=1612396800&v=beta&t=DY6vvIilCdJjiCLkJZkueB_BBCJAbmzZYX73ZPynvXQ",
    },
    {
      company: 'Okadoc',
      from: 'Feb 2019',
      to: 'Oct 2020',
      title: 'Frontend Engineer',
      img: "https://cdn.techinasia.com/data/images/24a0d6d1171d3c49c6d6d5d6466e738c.jpg",
    },
    {
      company: 'VIP Plaza',
      from: 'Aug 2018',
      to: 'Jan 2019',
      title: 'Junior Frontend Engineer',
      img: "https://cf.shopee.co.id/file/b107ae78565f4893614f92e2e53f18f6",
    },
  ];

  return (
    <Fragment>
      <Row style={{ marginTop: '6em'}} className="mb-3 disable-mobile-view">
        <Col xs={12} md={5} lg={6} className="d-flex justify-content-center align-items-center">
          <span style={{
            color: '#206BC3',
            fontSize: 35
          }}>Experience</span>
        </Col>
        <Col md={1} lg={1} style={{
            display: 'flex',
            alignItems: 'center',
        }}>
            <div style={{
                borderLeft: '3px solid #D0E3F0',
                height: '338px'
            }}></div>
        </Col>
        <Col xs={12} md={6} lg={5} className="mt-4">
          {staticDataExp.map((v, k) => (
            <div style={{
              marginLeft: '2em',
              marginTop: '1em',
              marginBottom: '4em'
            }}>
              <Row>
                <Col xs={3} md={3} lg={3}>
                  <img
                    style={{ borderRadius: 10}}
                    width="70"
                    height="70"
                    src={v.img}
                  />
                </Col>
                <Col className="pl-4">
                  <div style={{ fontSize: 23, color: '#707070' }}>{v.company}</div>
                  <div style={{ fontSize: 13, color: '#707070', fontWeight: '530' }}>{v.title}</div>
                  <div style={{ fontSize: 12, color: '#707070'}}>{v.from} - {v.to}</div>
                </Col>
              </Row>
            </div>
          ))}
          
          <div style={{ textAlign: 'center', color: '#707070'}}>
              <span >View All</span>
          </div>
          </Col>
      </Row>
      <Row style={{ marginTop: '6em'}} className="mb-3 disable-desktop-view">
        <Col xs={12} md={12} lg={6}>
          <span style={{
            color: '#206BC3',
            fontSize: 35
          }}>Experience</span>
        </Col>
        <Col xs={12} md={12} lg={6} className="mt-4">
          {staticDataExp.map((v, k) => (
            <div style={{
              marginLeft: '2em',
              marginTop: '1em',
              marginBottom: '4em'
            }}>
              <Row>
                <Col xs={3} md={3} lg={3}>
                  <img
                    style={{ borderRadius: 10}}
                    width="70"
                    height="70"
                    src={v.img}
                  />
                </Col>
                <Col className="pl-4">
                  <div style={{ fontSize: 23, color: '#707070' }}>{v.company}</div>
                  <div style={{ fontSize: 13, color: '#707070', fontWeight: '530' }}>{v.title}</div>
                  <div style={{ fontSize: 12, color: '#707070'}}>{v.from} - {v.to}</div>
                </Col>
              </Row>
            </div>
          ))}
          
          <div style={{ textAlign: 'center', color: '#707070'}}>
                <span >View All</span>
            </div>
        </Col>
      </Row>
    </Fragment>
  )
}
