import { Row, Col } from 'react-bootstrap';
import Swiper from 'react-id-swiper';
import 'swiper/swiper.scss'
import { Fragment } from 'react';
const params = {
    slidesPerView: 2,
    spaceBetween: 30,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  }
export default ({

}) => (
  <Fragment>
    <Row style={{ marginTop: '6em'}} className="mb-3 disable-desktop-view">
      <Col xs={12} md={12} lg={12}>
        <span style={{
            color: '#206BC3',
            fontSize: 35
        }}>Specilizing In</span>
      </Col>
      <Col style={{ marginTop: '4em' }}>
        <Swiper {...params}>
          <div>
            <Fragment>
                  <div style={{
                      borderRadius: '10px',
                      border: '1px solid #087CCB',
                      width: '150px',
                      height: '228px',
                      display: 'flex',
                      paddingLeft: 10,
                      alignItems: 'center'
                  }}>
                      <div style={{
                          flexWrap: 'wrap',
                          width: '90%',
                          color: '#393939',
                          marginTop: 20
                      }}>
                          <span style={{ fontWeight: 'bold'}}>Backend Development</span>
                          <div style={{ fontSize: 13, paddingTop: 10 }}>
                              Node JS, Go Language, Python Django, Flask
                          </div>
                      </div>
                  </div>
              </Fragment>
              
          </div>
          
          <div>
            <Fragment>
                  <div style={{
                      borderRadius: '10px',
                      border: '1px solid #087CCB',
                      width: '150px',
                      height: '228px',
                      display: 'flex',
                      paddingLeft: 10,
                      alignItems: 'center'
                  }}>
                      <div style={{
                          flexWrap: 'wrap',
                          width: '90%',
                          color: '#393939',
                          marginTop: 20
                      }}>
                          <span style={{ fontWeight: 'bold'}}>Front End Development</span>
                          <div style={{ fontSize: 13, paddingTop: 10 }}>
                              React JS, Next.JS, Server side rendering, Angular 9
                          </div>
                      </div>
                  </div>
              </Fragment>
          </div>

          <div>
            <Fragment>
                  <div style={{
                      borderRadius: '10px',
                      border: '1px solid #087CCB',
                      width: '150px',
                      height: '228px',
                      display: 'flex',
                      paddingLeft: 10,
                      alignItems: 'center'
                  }}>
                      <div style={{
                          flexWrap: 'wrap',
                          width: '90%',
                          color: '#393939',
                          marginTop: 20
                      }}>
                          <span style={{ fontWeight: 'bold'}}>Mobile Development</span>
                          <div style={{ fontSize: 13, paddingTop: 10 }}>
                              React Native, Flutter
                          </div>
                      </div>
                  </div>
              </Fragment>
          </div>
          
        </Swiper>
      </Col>
    </Row>
    <Row className="disable-mobile-view" style={{ marginTop: '7em',  marginBottom: '6em'  }}>
        <Col lg={12} md={12}>
            <span style={{
                color: '#206BC3',
                fontSize: 35,
                display: 'flex',
                justifyContent: 'center'
            }}>Specilizing In</span>
        </Col>
        <Col lg={12} md={12} className="mt-5">
            <div style={{
                display: 'flex',
                justifyContent: 'center',
            }}>
                <div className="mt-3 ml-5 mr-5">
                    <Fragment>
                        <div style={{
                            borderRadius: '10px',
                            border: '1px solid #087CCB',
                            width: '220px',
                            height: '280px',
                            display: 'flex',
                            paddingLeft: 10,
                            alignItems: 'center'
                        }}>
                            <div style={{
                                flexWrap: 'wrap',
                                width: '80%',
                                color: '#393939',
                                marginTop: 20
                            }}>
                                <span style={{ fontWeight: 'bold'}}>Backend Development</span>
                                <div style={{ fontSize: 13, paddingTop: 10 }}>
                                    Node JS, Go Language, Python Django, Flask
                                </div>
                            </div>
                        </div>
                    </Fragment>
                </div>

                <div className="mt-3 ml-5 mr-5">
                    <Fragment>
                        <div style={{
                            borderRadius: '10px',
                            border: '1px solid #087CCB',
                            width: '220px',
                            height: '280px',
                            display: 'flex',
                            paddingLeft: 10,
                            alignItems: 'center'
                        }}>
                            <div style={{
                                flexWrap: 'wrap',
                                width: '80%',
                                color: '#393939',
                                marginTop: 20
                            }}>
                                <span style={{ fontWeight: 'bold'}}>Front End Development</span>
                                <div style={{ fontSize: 13, paddingTop: 10 }}>
                                    React JS, Next.JS, Server side rendering, Angular 9
                                </div>
                            </div>
                        </div>
                    </Fragment>
                </div>

                <div className="mt-3 ml-5 mr-5">
                    <Fragment>
                        <div style={{
                            borderRadius: '10px',
                            border: '1px solid #087CCB',
                            width: '220px',
                            height: '280px',
                            display: 'flex',
                            paddingLeft: 10,
                            alignItems: 'center'
                        }}>
                            <div style={{
                                flexWrap: 'wrap',
                                width: '80%',
                                color: '#393939',
                                marginTop: 20
                            }}>
                                <span style={{ fontWeight: 'bold'}}>Mobile Development</span>
                                <div style={{ fontSize: 13, paddingTop: 10 }}>
                                    React Native, Flutter
                                </div>
                            </div>
                        </div>
                    </Fragment>
                </div>
            </div>
        </Col>
    </Row>
  </Fragment>
)