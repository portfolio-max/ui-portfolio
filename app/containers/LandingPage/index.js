import ProfileUser from '@containers/LandingPage/section/Profile'
import Experience from '@containers/LandingPage/section/Experience'
import SpecilizingIn from '@containers/LandingPage/section/Specilizing'

export default ({

}) => (
    <div className="landing-page">
        <ProfileUser />
        <Experience />
        <SpecilizingIn />
    </div>
);