import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducer from '@redux/reducers';
import sagas from '@redux/sagas';

export default (initialState) => {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
    );

    store.runSaga = () => {
    // Avoid running twice
    if (store.saga) return;
    store.saga = sagaMiddleware.run(sagas);
    };

    store.stopSaga = async () => {
    // Avoid running twice
    if (!store.saga) return;
    store.dispatch(END);
    await store.saga.done;
    store.saga = null;
    };

    store.execSagaTasks = async (isServer, tasks) => {
    // run saga
    store.runSaga();
    // dispatch saga tasks
    tasks(store.dispatch);
    // Stop running and wait for the tasks to be done
    await store.stopSaga();
    // // Re-run on client side
    if (!isServer) {
        store.runSaga();
    }
    };

    // store.execSagaTasks = async (isServer, tasks) => store.runSaga();
    // Initial run
    store.runSaga();

    return store;
}