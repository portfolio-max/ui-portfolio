import Types from './types';

const requestProfile = params => ({ type: Types.REQUEST_PROFILE, params });
const receiveProfile = data => ({ type: Types.RECEIVE_PROFILE, data });


export default {
  requestProfile,
  receiveProfile,
};
