import { createTypes } from 'reduxsauce';

export default createTypes(`
  REQUEST_PROFILE
  RECEIVE_PROFILE
`);
