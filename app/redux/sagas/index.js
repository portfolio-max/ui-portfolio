import { takeLatest } from 'redux-saga/effects'
import API from '../../services/index';
import Types from '../actions/types';
import {
  get
} from './masterSaga';

const api = API.create();

export default function* init(){
    yield [
        takeLatest(Types.REQUEST_PROFILE, get, api),
    ]
}