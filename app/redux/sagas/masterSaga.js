import { call, put } from 'redux-saga/effects';
import actions from '../actions/index';

export function* get(api, action) {
    try {
        const data = yield call(api.getProfile, action.payload.userId);
        if(data){
            yield put(actions.receiveProfile(data))
        }
    } catch (e) {
        console.error('error: ',e);
    }
}
