import _ from 'lodash';
import Types from '../actions/types';
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
  loading: false,
  data: {}
};

const request = state =>
  Object.assign({}, state, { loading: true });

const receive = (state, { data }) =>
   Object.assign({}, state, {
     loading: false,
     data: data
   });

const ACTION_HANDLERS = {
  [Types.REQUEST_PROFILE]: request,
  [Types.REQUEST_PROFILE]: receive
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);