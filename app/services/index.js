import apisauce from 'apisauce';
// import { API_HOST } from '../configs/api';

const create = (baseURL = process.env.API_HOST) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      "Content-Type":"application/json",
    },
    timeout: 10240
  });

  const getSliders = () => api.get('/profiles');

  return {
    getSliders
  };
};

export default {
  create
};
