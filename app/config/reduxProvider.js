import { Provider } from 'react-redux';
import HOCRedux from '../libs/with-redux-store';

const ReduxProvider = ({ children, ...rest }) => {
	const { store } = rest;
	return (
		<Provider store={store}>
			{ children }
    	</Provider>
	)
}

export default HOCRedux(ReduxProvider);