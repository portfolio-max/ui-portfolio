import React from 'react';
import Swiper from 'react-id-swiper';
import CardList from '../Cards/cardList';
import data from '../../../static/skils.json';

import './styles.scss';

class ExperienceSkill extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      skills: '',
    };
  }

  componentDidMount() {

  }

  render() {
    return (
      <div>
        <div
          className="container exp__index"
          style={{ marginTop: '10%', textAlign: 'center' }}
        >
          <h3 className="title" style={{ color: '#8C9192', marginBottom: '0px' }}><b>Skills</b></h3>
          <div style={{
            display: 'flex', content: ' ', justifyContent: 'center', flexWrap: 'wrap', alignItems: 'center',
          }}
          >
            {
              data.map((value, key) => (
                <CardList
                  key={key}
                  icons={value.icons}
                  bgColor={value.bgColor}
                  btnColor={value.btnColor}
                  id={value.id}
                  framework={value.framework}
                />
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

export default ExperienceSkill;
