import Head from 'next/head';
import PropTypes from 'prop-types';
import Navbar from '../Navbar';
import Footer from '../Footer';

import { Fragment } from 'react';

const Layout = ({ children, title = '' }) => (
  <Fragment>
    <Head>
      <title>{title}</title>
      {/* Open Graph Meta */}
      <meta property="og:title" content="Maximillian Nico Lolong || Frontend Engineer at Tiket.com" />
      <meta property="og:url" content="https://maximillianonico.dev" />
      <meta property="og:image" content="https://maximillianonico.dev/static/pict.jpg" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Navbar />
    <div className="container">
      {children}
    </div>
    <Footer />
  </Fragment>
);

Layout.PropTypes = {
  title: PropTypes.string.isRequired,
};

export default Layout;
