import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const cardList = ({
  icons, bgColor, id, btnColor, framework,
}) => (
  <div className="cardlist__index">
    <i style={{ fontSize: '100px', color: bgColor }} className={icons} />
    <div className="cardlist__detail">
      <h6 style={{ margin: '0px' }}>{id}</h6>
      <p className="desc" style={{ marginBottom: '5px' }}>Point: 9/10</p>
      <p className="desc" style={{ marginBottom: '5px' }}>
Framework:
        {framework}
      </p>
    </div>
  </div>
);

cardList.PropTypes = {
  icons: PropTypes.string,
  bgColor: PropTypes.string,
  id: PropTypes.string,
};
export default cardList;
