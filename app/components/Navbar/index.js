import React from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'react-bootstrap'

import './styles.scss';

const NavBarTop = () => {
  const redirect = (url = '') => {
    window.open(url, '_blank');
  }
  return (
    <Container>
      <Row>
        <Col md={6} xs={6} style={{
            float: 'left',
            display: 'flex',
            alignItems: 'center'
          }}>
          <h3 className="navbar__title pl-lg-5">Max</h3>
        </Col>
        <Col md={6} xs={6} style={{
            float: 'right',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <div className="" >
              <i
                style={{ padding: '0px 20px 2px 10px', fontSize:20, cursor: 'pointer' }}
                className="fab fa-linkedin-in"
                onClick={() => redirect('https://www.linkedin.com/in/maximilliano-nico-lolong-3b976b136/')}
              />
              <i
                style={{ padding: '0px 20px 2px 10px', fontSize:20, cursor: 'pointer' }}
                className="fab fa-instagram"
              />
              <i 
                style={{ padding: '0px 20px 2px 10px', fontSize:20, cursor: 'pointer' }}
                className="fab fa-twitter"
                onClick={() => redirect('https://twitter.com/maximilliano_1')}
              />
            </div>
        </Col>
      </Row>
    </Container>
  )
};

export default NavBarTop;
