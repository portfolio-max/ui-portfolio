import { Container, Row, Col } from 'react-bootstrap';
import { Fragment } from "react";

export default () => {
  const mailTo = () => {
    window.location = "mailto:maximillianonico8@gmail.com"
  }
  return (
    <Fragment>
      <div className="disable-desktop-view">
        <div className="footer">
          <div className="footer__title">Max</div>
          <div className="footer__desc">
              I’m looking for project Web Dev and Mobile Development, if you interest. You can mail :)
          </div>
          <button className="footer__button-hire">
              Hire Me
          </button>

          <hr style={{ backgroundColor: 'white', marginTop: 30, marginBottom: 10}}/>
          <span style={{ fontSize: 14, color: 'white' }}>@copyright 2019 Maximilliano</span>
        </div>
      </div>
      <div className="disable-mobile-view footer">
          <Container>
            <Row>
              <Col lg={3} md={3}>
                <div className="footer__title">Max</div>
              </Col>
              <Col lg={3} md={3}>
                <div className="footer__desc" style={{ flexWrap: 'wrap'}}>
                Software Engineer who mostly works as the full stack developer. Skills in designing and developing API, for server-client, created frontend with HTML, CSS Framework and ReactJs.
                </div>
              </Col>
              <Col lg={3} md={3} style={{
                  color: 'white',
                  textAlign: 'center',
                  fontSize: 15,
              }}>
                <div className="mt-2 mb-3">Homepage</div>
                <div className="mt-2 mb-3">Article</div>
                <div className="mt-2 mb-3">Project</div>
                <div className="mt-2 mb-3">Contact</div>
              </Col>
              <Col lg={3} md={3}>
                <div className="footer__desc">
                  I’m looking for project Web Dev and Mobile Development, if you interest. You can mail :)
                </div>
                <button className="footer__button-hire" onClick={mailTo}>
                  Hire Me
                </button>
              </Col>
            </Row>

            <hr style={{ backgroundColor: 'white', marginTop: 30, marginBottom: 10}}/>
            <span style={{ fontSize: 14, color: 'white' }}>@copyright 2019 Maximilliano</span>
          
          </Container>
        </div>
    </Fragment>
  )
}