const staticPage = () => {
  return {
    '/': { page: '/index' },
  };
};

module.exports = [
  { slug: '/', path: '/' },
  { slug: '/covid-19', path: '/feature'}
];
  