import React from 'react';
import Layout from '@components/Layout';
import PersonalityContainer from '@containers/Personality';

import './styles.scss';

class Index extends React.Component {
  render() {
    return (
      <Layout title="App Personality Insight">
          <PersonalityContainer {...this.props} />
      </Layout>
    );
  }
}

export default Index;
