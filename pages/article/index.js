import React from 'react';

import Layout from '@components/Layout';

class Article extends React.Component {
  render() {
    return (
      <Layout title="article - Max">
        <div>Article</div>
      </Layout>
    );
  }
}

export default Article;
