import React from 'react';

import ExperienceSkill from '@components/Experience';
import Layout from '@components/Layout';
import LandingPage from '@containers/LandingPage'

import './styles.scss';

class Index extends React.Component {
  render() {
    return (
      <Layout title="Maximilliano | Full Stack Developer">
        <LandingPage />
      </Layout>
    );
  }
}

export default Index;
