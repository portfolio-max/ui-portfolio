/* eslint-disable import/no-unresolved */
/* eslint-disable no-underscore-dangle */
import App from 'next/app';
import { Provider } from 'react-redux';
import WrapperHOC from '@libs/with-redux-store';
import { register, unregister } from 'next-offline/runtime';
import { Fragment } from 'react';
import '../static/styles/App.scss';
const theme = {
  btnSuccess: 'green',
};
class AppWrapper extends App {
  static async getInitialProps({ Component, ctx, store }, config) {
    const isServer = !!ctx.req;
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx, { ...config, isServer })
      : {};
    return { pageProps, namespacesRequired: ['common'] };
  }

  // componentDidMount() {
  //   register();
  // }

  // componentWillUnmount() {
  //   unregister();
  // }

  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <Fragment>
          <Component {...pageProps} />
      </Fragment>
    );
  }
}

export default WrapperHOC(AppWrapper);
