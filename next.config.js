// dotenv
require('dotenv').config()

// workbox webpack
const WorkboxPlugin = require('workbox-webpack-plugin')
// withSass
const withSass = require('@zeit/next-sass')
const withCss = require('@zeit/next-css')
const withPlugins = require('next-compose-plugins');
const path = require('path')
const Dotenv = require('dotenv-webpack')

const nextConfig = {
  webpack: (config) => {
    config.plugins = config.plugins || []

    config.plugins = [
      ...config.plugins,

      // Read the .env file
      new Dotenv({
        path: path.join(__dirname, '.env'),
        systemvars: true
      })
    ]

    config.plugins.push(new WorkboxPlugin.GenerateSW())

    return config
  }
}

module.exports = withPlugins([
  [withSass],[withCss]
], nextConfig);