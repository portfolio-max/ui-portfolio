module.exports = {
    "extends": "airbnb",
    "parser": "babel-eslint",
    "plugins":[
        "react",
        "jsx-a11y",
        "import"
    ],
    "react/prefer-stateless-function": [2, { "ignorePureComponents": true }],
};